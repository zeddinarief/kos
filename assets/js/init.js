(function($){
  $(function(){

    $('.sidenav').sidenav();
    $('.parallax').parallax();
	$('.datepicker').datepicker({
		format : "yyyy-mm-dd",
		autoClose : true
	});
	$('.owl-carousel').owlCarousel({
		loop: false,
		margin: 10,
		responsiveClass: true,
		autoHeight:true,
		responsive: {
		  0: {
			items: 1,
			nav: true
		  },
		  600: {
			items: 2,
			nav: false
		  },
		  1000: {
			items: 3,
			nav: false,
			loop: false,
			margin: 10
		  }
		}
	  });
	$('.carousel').carousel();
	$('.carousel.carousel-slider').carousel();
	$('.modal').modal();
	$('.dropdown-trigger').dropdown();
	$('select').formSelect();
	$('.pushpin-demo-nav').each(function() {
    	var $this = $(this);
    	var $target = $('#' + $(this).attr('data-target'));
    	$this.pushpin({
      		top: $target.offset().top,
      		bottom: $target.offset().top + $target.outerHeight() - $this.height()
    	});
  	});

  }); // end of document ready
})(jQuery); // end of jQuery name space
